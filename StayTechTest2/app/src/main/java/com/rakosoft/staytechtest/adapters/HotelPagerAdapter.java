package com.rakosoft.staytechtest.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rakosoft.staytechtest.R;
import com.rakosoft.staytechtest.activities.HotelActivity;
import com.rakosoft.staytechtest.beans.hotel.Hotel;
import com.rakosoft.staytechtest.utils.Utils;

import java.util.ArrayList;

/**
 * Created by Joss on 15/06/2016.
 */
public class HotelPagerAdapter extends PagerAdapter {

    private Activity mActivity;
    private LayoutInflater mLayoutInflater;
    private ArrayList<Hotel> hotelList;
    private boolean isPortrait;

    public HotelPagerAdapter(Activity activity, ArrayList<Hotel> hotelList) {
        mActivity = activity;
        mLayoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.hotelList=hotelList;
        isPortrait= Utils.getScreenOrientation(mActivity)== Configuration.ORIENTATION_PORTRAIT;
    }

    @Override
    public int getCount() {
        return hotelList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.page_hotel, container, false);

        TextView tvHotelName = (TextView) itemView.findViewById(R.id.tvHotelName);
        tvHotelName.setText(hotelList.get(position).getName());

        TextView tvHotelAddress = (TextView) itemView.findViewById(R.id.tvHotelAddress);
        tvHotelAddress.setText(hotelList.get(position).getMainAddress().getAddress());

        ImageView imgHotel = (ImageView) itemView.findViewById(R.id.imgHotel);
        //imgHotel.setScaleType(isPortrait?ImageView.ScaleType.CENTER_CROP:ImageView.ScaleType.CENTER_INSIDE);
        if(hotelList.get(position).getImgHotel()!=null){
            imgHotel.setImageBitmap(hotelList.get(position).getImgHotel());
        }else{
            imgHotel.setImageResource(R.drawable.hotel_default_image);
        }

        Button btnMap = (Button) itemView.findViewById(R.id.btnMap);
        try{
            final String name = hotelList.get(position).getName();
            final double lat = Double.parseDouble(hotelList.get(position).getCoordinates().getLatitude());
            final double lng = Double.parseDouble(hotelList.get(position).getCoordinates().getLongitude());
            btnMap.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    goToMap(name,lng,lat);
                }
            });
        }catch(Exception e){
            Log.e("Error","Error en: "+e.getMessage());
            btnMap.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Toast.makeText(mActivity,mActivity.getResources().getString(R.string.error_coordinates),Toast.LENGTH_SHORT).show();
                }
            });
        }


        Button btnVisit = (Button) itemView.findViewById(R.id.btnVisit);
        btnVisit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                callCreateVisitor();
            }
        });
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    public void goToMap(String nameHotel, double lng, double lat){
        ((HotelActivity)mActivity).goToMap(nameHotel,lng,lat);
    }

    public void callCreateVisitor(){
        ((HotelActivity)mActivity).callCreateVisitor();
    }

}