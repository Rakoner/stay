package com.rakosoft.staytechtest.network;

/**
 * Created by Joss on 12/06/2016.
 */
public class NetworkConstants {

    //TODO this must be in flavours for int pre pro etc environtments, not mocked like this
    public static final String BASE_URL_SERVER="https://api-int.stay-app.com/v1/";
    public static final String HOTELS_URL ="establishments/list"+NetworkConstants.STAY_STRING_API_KEY;
    //public static final String HOTEL_IMAGE_URL =BASE_URL_SERVER+"images/get/10065/original"+NetworkConstants.STAY_STRING_API_KEY;
    public static final String HOTEL_IMAGE_URL_1 ="images/get/";
    public static final String HOTEL_IMAGE_URL_2 ="/original";
    public static final String CREATE_VISITOR_URL ="users/visitor/create"+NetworkConstants.STAY_STRING_API_KEY;
    public static final String STAY_STRING_API_KEY="?apikey="+NetworkConstants.STAY_API_KEY;
    public static final String STAY_API_KEY="684939f31f971f9ac0a1254b13cd927b";


    public static final String CALLSERVERGETHOTELS="callServerGetHotels";
}
