package com.rakosoft.staytechtest.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.TextView;

import com.rakosoft.staytechtest.R;
import com.rakosoft.staytechtest.beans.hotel.Hotel;
import com.rakosoft.staytechtest.controller.Controller;
import com.rakosoft.staytechtest.network.NetworkUtils;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Joss on 13/06/2016.
 */
public class HotelAdapter extends RecyclerView.Adapter<HotelAdapter.HotelsViewHolder>  implements View.OnClickListener {

    private ArrayList<Hotel> hotelsList;
    private String filter;
    private Context mContext;
    private View.OnClickListener listener;

    public HotelAdapter(ArrayList<Hotel> hotelList, String filter, Context aCtx) {
        this.hotelsList = hotelList;
        this.filter = filter;
        this.mContext=aCtx;
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if(listener != null){
            listener.onClick(v);
        }
    }

    @Override
    public HotelsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_hotel, parent, false);
        itemView.setOnClickListener(this);
        return new HotelsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(HotelsViewHolder holder, int position) {
        Hotel item = hotelsList.get(position);
        holder.bindHotel(item,position,filter);
    }

    @Override
    public int getItemCount() {
        return hotelsList.size();
    }

    public static class HotelsViewHolder extends RecyclerView.ViewHolder {

        private com.rakosoft.staytechtest.customizedviews.ImageViewRoundedFitWidth imgHotel;
        private TextView tvHotelName;

        public HotelsViewHolder(View itemView) {
            super(itemView);
            tvHotelName = (TextView)itemView.findViewById(R.id.tvHotelName);
            imgHotel = (com.rakosoft.staytechtest.customizedviews.ImageViewRoundedFitWidth)itemView.findViewById(R.id.imgHotel);
        }

        public void bindHotel(Hotel aHotel,int aPosition, String aFilter) {
            tvHotelName.setText(aHotel.getName());
            //Image Universal Loader Option
            //ImageUtils.loadImage(aCtx, NetworkConstants.HOTEL_IMAGE_URL,imgHotel, Constants.DEFAULT_DISPLAY_IMAGE_OPTIONS);

            //Picasso Option
//            OkHttpClient okHttpClient = new OkHttpClient();
//            okHttpClient.interceptors().add(new PicassoInterceptor());
//            OkHttpDownloader downloader = new OkHttpDownloader(okHttpClient);
//            new OkHttpDownloader(okHttpClient);

            //Asynktask Option
            if(aHotel.getImgHotel()!=null){
                imgHotel.setImageBitmap(aHotel.getImgHotel());
            }else{
                String urlHotelImage=NetworkUtils.getImageURL(aHotel.getAutologinTemplate().getImages().getMAIN_BACKGROUND_LOGIN_IMAGE().getImage());
                if(URLUtil.isValidUrl(urlHotelImage)){
                    new LoadImage(imgHotel,urlHotelImage,aPosition, aFilter).execute();
                }else{
                    imgHotel.setImageResource(R.drawable.hotel_default_image);
                }
            }

        }
    }

    private static class LoadImage extends AsyncTask<Void, Void, Bitmap> {

        ImageView imageView;
        String url;
        int position;
        String filter;

        public LoadImage(ImageView imageView, String url, int position, String filter){
            this.imageView=imageView;
            this.url=url;
            this.position=position;
            this.filter=filter;
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected Bitmap doInBackground(Void... params) {
            Bitmap bitmap=null;
            URL imageURL = null;
            try {
                imageURL = new URL(url);
            }catch (MalformedURLException e) {
                e.printStackTrace();
            }
            try {
                HttpURLConnection connection = (HttpURLConnection) imageURL
                        .openConnection();
                connection.setDoInput(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                JSONObject empty   = new JSONObject();
                OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
                wr.write(empty.toString());
                wr.flush();
                InputStream inputStream = connection.getInputStream();
                bitmap = BitmapFactory.decodeStream(inputStream);// Convert to
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            imageView.setImageBitmap(result);
            if(result!=null){
                imageView.setImageBitmap(result);
                Controller.getInstance().getFilteredHotels(filter).get(position).setImgHotel(result);
            }else{
                imageView.setImageResource(R.drawable.hotel_default_image);
            }
        }

    }

//    private static class PicassoInterceptor implements Interceptor {
//
//        @Override
//        public Response intercept(Chain chain) throws IOException {
//
//            final MediaType JSON
//                    = MediaType.parse("application/json; charset=utf-8");
//            Map<String, String> map = new HashMap<String, String>();
////        map.put("session_id", session_id);
////        map.put("image", image);
//            String requestJsonBody = new Gson().toJson(map);
//            RequestBody body = RequestBody.create(JSON, requestJsonBody);
//            final Request original = chain.request();
//            final Request.Builder requestBuilder = original.newBuilder()
//                    .url(NetworkConstants.HOTEL_IMAGE_URL)
//                    .post(body);
//            return chain.proceed(requestBuilder.build());
//        }
//    }



}