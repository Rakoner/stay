package com.rakosoft.staytechtest.network.services;

import com.rakosoft.staytechtest.network.NetworkConstants;
import com.rakosoft.staytechtest.network.request.CreateVisitorRequest;
import com.rakosoft.staytechtest.network.response.CreateVisitorResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Joss on 20/06/2016.
 */
public interface VisitorService {

    @Headers("Content-Type: application/json")
    @POST(NetworkConstants.CREATE_VISITOR_URL)
    Call<CreateVisitorResponse> createVisitor(@Body CreateVisitorRequest body);

}
