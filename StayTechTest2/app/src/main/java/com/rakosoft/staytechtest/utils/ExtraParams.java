package com.rakosoft.staytechtest.utils;

/**
 * Created by Joss on 15/06/2016.
 */
public class ExtraParams {
    public static String EXTRA_HOTEL_POSITION="EXTRA_HOTEL_POSITION";
    public static String EXTRA_HOTEL_FILTER="EXTRA_HOTEL_FILTER";
    public static String EXTRA_HOTEL_MAP_NAME="EXTRA_HOTEL_MAP_NAME";
    public static String EXTRA_HOTEL_MAP_LONG="EXTRA_HOTEL_MAP_LONG";
    public static String EXTRA_HOTEL_MAP_LAT="EXTRA_HOTEL_MAP_LAT";

}
