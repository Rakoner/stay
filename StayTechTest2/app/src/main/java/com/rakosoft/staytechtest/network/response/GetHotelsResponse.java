package com.rakosoft.staytechtest.network.response;

import com.rakosoft.staytechtest.beans.hotel.Hotel;
import java.util.ArrayList;

/**
 * Created by Joss on 12/06/2016.
 */
public class GetHotelsResponse extends BasicResponse{

    private ArrayList<Hotel> data;

    public ArrayList<Hotel> getHotels() {
        return data;
    }

    public void setHotels(ArrayList<Hotel> hotels) {
        this.data = hotels;
    }
}
