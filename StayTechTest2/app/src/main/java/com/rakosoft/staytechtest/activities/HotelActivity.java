package com.rakosoft.staytechtest.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.rakosoft.staytechtest.R;
import com.rakosoft.staytechtest.adapters.HotelPagerAdapter;
import com.rakosoft.staytechtest.controller.Controller;
import com.rakosoft.staytechtest.network.NetworkUtils;
import com.rakosoft.staytechtest.services.VisitorsService;
import com.rakosoft.staytechtest.utils.ExtraParams;
import com.rakosoft.staytechtest.utils.Utils;


/**
 * Created by Joss on 15/06/2016.
 */
public class HotelActivity extends Activity {

    private ViewPager mViewPager;
    private HotelPagerAdapter mHotelPagerAdapter;
    private int hotelPositionSelected;
    private String hotelFilterText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel);
        Bundle b = getIntent().getExtras();
        hotelPositionSelected = b.getInt(ExtraParams.EXTRA_HOTEL_POSITION);
        hotelFilterText = b.getString(ExtraParams.EXTRA_HOTEL_FILTER);
        mHotelPagerAdapter = new HotelPagerAdapter(this, Controller.getInstance().getFilteredHotels(hotelFilterText));

        mViewPager = (ViewPager) findViewById(R.id.pagerHotel);
        mViewPager.setAdapter(mHotelPagerAdapter);
        mViewPager.setCurrentItem(hotelPositionSelected);
    }

    public void goToMap(String nameHotel, double lng, double lat){
        Intent intent = new Intent(this, MapStayActivity.class);
        intent.putExtra(ExtraParams.EXTRA_HOTEL_MAP_NAME,nameHotel);
        intent.putExtra(ExtraParams.EXTRA_HOTEL_MAP_LAT,lat);
        intent.putExtra(ExtraParams.EXTRA_HOTEL_MAP_LONG,lng);
        startActivity(intent);
    }

    public void callCreateVisitor(){
        if(NetworkUtils.isDeviceOnline(HotelActivity.this)){
            new VisitorsService().callServerCreateVisior(HotelActivity.this);
        }else{
            Utils.showInfoDialog(getResources().getString(R.string.internet_no_connection), HotelActivity.this);
        }
    }

}
