package com.rakosoft.staytechtest.network.response;

import android.graphics.Bitmap;

/**
 * Created by Joss on 20/06/2016.
 */
public class GetHotelImageResponse {

    private Bitmap bitmap;

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}
