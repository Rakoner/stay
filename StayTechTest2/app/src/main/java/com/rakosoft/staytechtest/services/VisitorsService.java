package com.rakosoft.staytechtest.services;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.rakosoft.staytechtest.R;
import com.rakosoft.staytechtest.network.NetworkConstants;
import com.rakosoft.staytechtest.network.NetworkUtils;
import com.rakosoft.staytechtest.network.request.CreateVisitorRequest;
import com.rakosoft.staytechtest.network.response.CreateVisitorResponse;
import com.rakosoft.staytechtest.network.services.ServiceGenerator;
import com.rakosoft.staytechtest.network.services.VisitorService;
import com.rakosoft.staytechtest.utils.Utils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Joss on 20/06/2016.
 */
public class VisitorsService {

    public void callServerCreateVisior(final Activity activity){
        VisitorService visitorsService = ServiceGenerator.createService(VisitorService.class, activity,activity.getResources().getString(R.string.loading));
        CreateVisitorRequest request = new CreateVisitorRequest();
        request.setCountry("es");
        request.setOs("ios");
        request.setVersion("7.0.3");
        request.setModel("iphone4");
        request.setLanguage("es");
        request.setPushToken("a2340239329293443");
        request.setDeviceUid("d399f3993399d");
        Call<CreateVisitorResponse> call = visitorsService.createVisitor(request);
        call.enqueue(new Callback<CreateVisitorResponse>() {
            @Override
            public void onResponse(Call<CreateVisitorResponse> call, Response<CreateVisitorResponse> response) {
                Log.i(activity.getResources().getString(R.string.retrofit_log), activity.getResources().getString(R.string.retrofit_onResponse, NetworkConstants.CALLSERVERGETHOTELS,activity.getClass().getSimpleName()));
                CreateVisitorResponse createLinkObjectResponse = response.body();
                if(createLinkObjectResponse!=null){
                    if(createLinkObjectResponse.isSuccess()){
                        //new HotelsService().processGetHotelsServerCall(createLinkObjectResponse);
                        //((MainActivity)activity).showHotels();
                        Toast.makeText(activity.getApplicationContext(),"Visitor created with this data: "+createLinkObjectResponse.getVisitor(),Toast.LENGTH_SHORT).show();
                        NetworkUtils.dismissProgress(activity);
                    }else{
                        ArrayList<String> errors = createLinkObjectResponse.getErrors();
                        if(errors!=null){
                            //TODO make this better....
                            for(String error: errors){
                                Toast.makeText(activity.getApplicationContext(),error,Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(activity.getApplicationContext(),activity.getResources().getString(R.string.generic_error),Toast.LENGTH_SHORT).show();
                        }
                    }
                }else{
                    Log.i(activity.getResources().getString(R.string.retrofit_log), activity.getResources().getString(R.string.retrofit_onFailure, NetworkConstants.CALLSERVERGETHOTELS,activity.getClass().getSimpleName()));
                }
                //((MainActivity)activity).hideSoftKeyboard();
            }

            @Override
            public void onFailure(Call<CreateVisitorResponse> call, Throwable t) {
                Log.i(activity.getResources().getString(R.string.retrofit_log), activity.getResources().getString(R.string.retrofit_onFailure, NetworkConstants.CALLSERVERGETHOTELS,activity.getClass().getSimpleName()));
                Utils.showInfoDialog(activity.getResources().getString(R.string.retrofit_error, NetworkConstants.CALLSERVERGETHOTELS), activity);
                System.out.println(call);
                //((MainActivity)activity).hideSoftKeyboard();
            }
        });
    }

}
