package com.rakosoft.staytechtest.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.rakosoft.staytechtest.R;
import com.rakosoft.staytechtest.utils.Constants;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_splash);
        createContentView();
        doSplashStuff();
    }

    public void doSplashStuff() {
        //TODO splash things like initialize libraries or download data
        waitAndGo();
    }

    public void waitAndGo() {
        // Execute some code after 2.5 seconds have passed
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                goToMainActivity();
            }
        }, Constants.SPLASH_FREEZE_TIME);
    }

    public void goToMainActivity(){
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    public void createContentView(){
        RelativeLayout relSplash = new RelativeLayout(this);
        relSplash.setBackgroundResource(R.drawable.bg_gradient);
        ImageView imgLogoSplash = new ImageView(this);
        imgLogoSplash.setImageResource(R.drawable.stay_logo);
        RelativeLayout.LayoutParams layoutParams =
                new  RelativeLayout.LayoutParams(500,500);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        imgLogoSplash.setLayoutParams(layoutParams);
        relSplash.addView(imgLogoSplash);
        setContentView(relSplash);
    }
}
