package com.rakosoft.staytechtest.controller;

import android.content.Context;

import com.rakosoft.staytechtest.beans.hotel.Hotel;

import java.util.ArrayList;

/**
 * Created by Joss on 13/06/2016.
 */
public class Controller {

    private static Controller ourInstance;
    private Context myContext;
    private ArrayList<Hotel> hotels;

    public static Controller getInstance() {
        if (ourInstance == null) {
            ourInstance = new Controller();
        }
        return ourInstance;
    }

//    public static void create(Context aCtx) {
//        ourInstance = new Controller();
//        ourInstance.setContext(aCtx);
//    }

    public void setContext(Context myContext) {
        this.myContext = myContext;
    }

    public ArrayList<Hotel> getHotels() {
        return hotels;
    }

    public void setHotels(ArrayList<Hotel> hotels) {
        this.hotels = hotels;
    }

    public ArrayList<Hotel> getPublishedHotels() {
        ArrayList<Hotel> result = new ArrayList<Hotel>();
        if(getHotels()!=null) {
            for(Hotel hotel: getHotels()){
                if(hotel.isPublished()){
                    result.add(hotel);
                }
            }
        }
        return result;
    }

    public ArrayList<Hotel> getFilteredHotels(String filter) {
        ArrayList<Hotel> result;
        if(filter.length()==0){
            result = getPublishedHotels();
        }else{
            result = new ArrayList<>();
            if(getPublishedHotels()!=null){
                for(Hotel hotel: getPublishedHotels()){
                    if(hotel.getName().toLowerCase().contains(filter)){
                        result.add(hotel);
                    }
                }
            }
        }
        return result;
    }
}
