package com.rakosoft.staytechtest.beans.hotel;

/**
 * Created by Joss on 17/06/2016.
 */
public class HotelCoordinates {

    private String latitude;
    private String longitude;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
