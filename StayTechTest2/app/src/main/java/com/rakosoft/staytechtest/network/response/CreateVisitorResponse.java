package com.rakosoft.staytechtest.network.response;

import com.rakosoft.staytechtest.beans.visitor.Visitor;

/**
 * Created by Joss on 20/06/2016.
 */
public class CreateVisitorResponse  extends BasicResponse{

    private Visitor data;

    public Visitor getVisitor() {
        return data;
    }

    public void setVisitor(Visitor visitor) {
        this.data = visitor;
    }
}
