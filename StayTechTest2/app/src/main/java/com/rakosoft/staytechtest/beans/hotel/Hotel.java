package com.rakosoft.staytechtest.beans.hotel;

import android.graphics.Bitmap;

/**
 * Created by Joss on 17/06/2016.
 */
public class Hotel implements Comparable<Hotel>{

    private String name;
    private boolean published;
    private HotelAutoLogin autologinTemplate;
    private HotelAddress mainAddress;
    private HotelCoordinates coordinates;
    private Bitmap imgHotel;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public HotelAutoLogin getAutologinTemplate() {
        return autologinTemplate;
    }

    public void setAutologinTemplate(HotelAutoLogin autologinTemplate) {
        this.autologinTemplate = autologinTemplate;
    }

    public HotelAddress getMainAddress() {
        return mainAddress;
    }

    public void setMainAddress(HotelAddress mainAddress) {
        this.mainAddress = mainAddress;
    }

    public HotelCoordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(HotelCoordinates coordinates) {
        this.coordinates = coordinates;
    }

    public Bitmap getImgHotel() {
        return imgHotel;
    }

    public void setImgHotel(Bitmap imgHotel) {
        this.imgHotel = imgHotel;
    }

    @Override
    public int compareTo(Hotel other) {
        return this.getName().compareTo(other.getName());
    }

    @Override
    public String toString() {
        return "Hotel{" +
                "name='" + name + '\'' +
                ", published=" + published +
                ", autologinTemplate=" + autologinTemplate +
                ", mainAddress=" + mainAddress +
                ", coordinates=" + coordinates +
                '}';
    }
}
