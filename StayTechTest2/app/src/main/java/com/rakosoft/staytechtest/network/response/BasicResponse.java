package com.rakosoft.staytechtest.network.response;

import java.util.ArrayList;

/**
 * Created by Joss on 17/06/2016.
 */
public class BasicResponse {

    protected ArrayList<String> errors;
    protected boolean success;

    public ArrayList<String> getErrors() {
        return errors;
    }

    public void setErrors(ArrayList<String> errors) {
        this.errors = errors;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
