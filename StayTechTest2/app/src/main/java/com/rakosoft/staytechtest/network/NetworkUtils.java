package com.rakosoft.staytechtest.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.rakosoft.staytechtest.utils.Utils;

/**
 * Created by Joss on 17/06/2016.
 */
public class NetworkUtils {

    public static void dismissProgress(Context aCtx){
        Utils.cancelProgressDialog(aCtx);
    }

    public static boolean isDeviceOnline(Context aCtx) {
        if(aCtx!=null){
            ConnectivityManager cm =
                    (ConnectivityManager)aCtx.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnectedOrConnecting();
        }else{
            return true;
        }
    }

    public static String getImageURL(int idImage){
        return NetworkConstants.BASE_URL_SERVER+NetworkConstants.HOTEL_IMAGE_URL_1+idImage+NetworkConstants.HOTEL_IMAGE_URL_2+NetworkConstants.STAY_STRING_API_KEY;
    }
}
