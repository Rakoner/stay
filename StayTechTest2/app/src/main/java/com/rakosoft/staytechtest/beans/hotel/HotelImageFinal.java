package com.rakosoft.staytechtest.beans.hotel;

/**
 * Created by Joss on 17/06/2016.
 */
public class HotelImageFinal {

    private int image;

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
