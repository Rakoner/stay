package com.rakosoft.staytechtest.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.rakosoft.staytechtest.R;
import com.rakosoft.staytechtest.adapters.HotelAdapter;
import com.rakosoft.staytechtest.beans.hotel.Hotel;
import com.rakosoft.staytechtest.controller.Controller;
import com.rakosoft.staytechtest.customizedviews.VerticalSpaceItemDecoration;
import com.rakosoft.staytechtest.network.NetworkUtils;
import com.rakosoft.staytechtest.services.HotelsService;
import com.rakosoft.staytechtest.utils.Constants;
import com.rakosoft.staytechtest.utils.ExtraParams;
import com.rakosoft.staytechtest.utils.Utils;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;

/**
 * Created by Joss on 12/06/2016.
 */
public class MainActivity extends Activity{

    private String screenName=this.getClass().getSimpleName();

    private HotelAdapter hotelAdapter;

    @Bind(R.id.recViewHotels)
    RecyclerView recViewHotels;

    @Bind(R.id.tvNoHotel)
    TextView tvNoHotel;

    @Bind(R.id.edFinder)
    EditText mEdFinder;


    @OnTextChanged(R.id.edFinder)
    protected void onFilterFinderChanged() {
        //hotelAdapter.notifyDataSetChanged();
        showHotels();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initHotels();
        getHotels();
    }

    @Override
    public void onResume() {
        super.onResume();
        tvNoHotel.requestFocus();
    }

    public void getHotels(){
        if(NetworkUtils.isDeviceOnline(MainActivity.this)){
            new HotelsService().callServerGetHotels(MainActivity.this);
        }else{
            Utils.showInfoDialog(getResources().getString(R.string.internet_no_connection), MainActivity.this);
        }
    }

    public void showHotels(){
        ArrayList<Hotel> filteredHotels = Controller.getInstance().getFilteredHotels(mEdFinder.getText().toString());
        if(filteredHotels.size()>0){
            recViewHotels.setVisibility(View.VISIBLE);
            tvNoHotel.setVisibility(View.GONE);
            hotelAdapter = new HotelAdapter(filteredHotels, mEdFinder.getText().toString(), MainActivity.this);
            recViewHotels.setAdapter(hotelAdapter);
            hotelAdapter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = recViewHotels.getChildAdapterPosition(v);
                    goToHotellDetail(position);
                }
            });
        }else{
            recViewHotels.setVisibility(View.GONE);
            tvNoHotel.setVisibility(View.VISIBLE);
        }
    }

    public void initHotels(){
        recViewHotels.setHasFixedSize(true);
        recViewHotels.addItemDecoration(new VerticalSpaceItemDecoration(Constants.VERTICAL_ITEM_SPACE));
        recViewHotels.setLayoutManager(
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false));
    }

    public void goToHotellDetail(int hotelPosSelected){
        Toast.makeText(MainActivity.this,"Pulsado el hotel: "+Controller.getInstance().getFilteredHotels(mEdFinder.getText().toString()).get(hotelPosSelected),Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, HotelActivity.class);
        intent.putExtra(ExtraParams.EXTRA_HOTEL_POSITION,hotelPosSelected);
        intent.putExtra(ExtraParams.EXTRA_HOTEL_FILTER,mEdFinder.getText().toString());
        startActivity(intent);
    }


}
