package com.rakosoft.staytechtest.beans.hotel;

/**
 * Created by Joss on 17/06/2016.
 */
public class HotelAddress {

    private String firstAddressLine;
    private String secondAddressLine;
    private String postalCode;
    private String country;
    private String city;
    private String region;

    public String getFirstAddressLine() {
        return firstAddressLine;
    }

    public void setFirstAddressLine(String firstAddressLine) {
        this.firstAddressLine = firstAddressLine;
    }

    public String getSecondAddressLine() {
        return secondAddressLine;
    }

    public void setSecondAddressLine(String secondAddressLine) {
        this.secondAddressLine = secondAddressLine;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getAddress(){
        return firstAddressLine+" "+secondAddressLine+" "+secondAddressLine+" "+postalCode+" "+country+" "+city+" "+region;
    }
}
