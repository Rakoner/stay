package com.rakosoft.staytechtest.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.rakosoft.staytechtest.R;
import com.rakosoft.staytechtest.utils.ExtraParams;

/**
 * Created by Joss on 20/06/2016.
*/
public class MapStayActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private String hotelName;
    private double hotelLat;
    private double hotelLong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        Bundle b = getIntent().getExtras();
        hotelName = b.getString(ExtraParams.EXTRA_HOTEL_MAP_NAME);
        hotelLat = b.getDouble(ExtraParams.EXTRA_HOTEL_MAP_LAT);
        hotelLong = b.getDouble(ExtraParams.EXTRA_HOTEL_MAP_LONG);
        Log.w("Coordinates","LAT: "+hotelLat+" y LONG: "+hotelLong);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng hotel = new LatLng(hotelLat, hotelLong);
        mMap.addMarker(new MarkerOptions().position(hotel).title("Marker in "+hotelName));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(hotel));
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                mMap.animateCamera(CameraUpdateFactory.zoomIn());
                mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
            }
        }, 1000);
    }

}