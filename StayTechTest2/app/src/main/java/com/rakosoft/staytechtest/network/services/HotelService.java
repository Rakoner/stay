package com.rakosoft.staytechtest.network.services;


import com.rakosoft.staytechtest.network.NetworkConstants;
import com.rakosoft.staytechtest.network.request.GetHotelsRequest;
import com.rakosoft.staytechtest.network.response.GetHotelsResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Joss on 12/06/2016.
 */
public interface HotelService {

    @Headers("Content-Type: application/json")
    @POST(NetworkConstants.HOTELS_URL)
    Call<GetHotelsResponse> getHotels(@Body GetHotelsRequest body);

//    @Headers("Content-Type: application/json")
//    @POST(NetworkConstants.HOTEL_IMAGE_URL)
//    Call<GetHotelsResponse> getHotelImage(@Body GetHotelsRequest body);
}
