package com.rakosoft.staytechtest.beans.visitor;

import java.util.ArrayList;

/**
 * Created by Joss on 20/06/2016.
 */
public class Visitor {

    private int age;
    private int appId;
    private boolean autologin;
    private String country;
    private String createdOn;
    private String deviceKey;
    private ArrayList<String> establishments;
    private boolean pmslogin;
    private boolean registeredUser;
    private String ts;
    private int uid;
    private String userKey;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAppId() {
        return appId;
    }

    public void setAppId(int appId) {
        this.appId = appId;
    }

    public boolean isAutologin() {
        return autologin;
    }

    public void setAutologin(boolean autologin) {
        this.autologin = autologin;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getDeviceKey() {
        return deviceKey;
    }

    public void setDeviceKey(String deviceKey) {
        this.deviceKey = deviceKey;
    }

    public ArrayList<String> getEstablishments() {
        return establishments;
    }

    public void setEstablishments(ArrayList<String> establishments) {
        this.establishments = establishments;
    }

    public boolean isPmslogin() {
        return pmslogin;
    }

    public void setPmslogin(boolean pmslogin) {
        this.pmslogin = pmslogin;
    }

    public boolean isRegisteredUser() {
        return registeredUser;
    }

    public void setRegisteredUser(boolean registeredUser) {
        this.registeredUser = registeredUser;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    @Override
    public String toString() {
        return "Visitor{" +
                "age=" + age +
                ", appId=" + appId +
                ", autologin=" + autologin +
                ", country='" + country + '\'' +
                ", createdOn='" + createdOn + '\'' +
                ", deviceKey='" + deviceKey + '\'' +
                ", establishments=" + establishments +
                ", pmslogin=" + pmslogin +
                ", registeredUser=" + registeredUser +
                ", ts='" + ts + '\'' +
                ", uid=" + uid +
                ", userKey='" + userKey + '\'' +
                '}';
    }
}
