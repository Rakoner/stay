package com.rakosoft.staytechtest.beans.hotel;

/**
 * Created by Joss on 17/06/2016.
 */
public class HotelImage {

    private HotelImageFinal MAIN_BACKGROUND_LOGIN_IMAGE;

    public HotelImageFinal getMAIN_BACKGROUND_LOGIN_IMAGE() {
        return MAIN_BACKGROUND_LOGIN_IMAGE;
    }

    public void setMAIN_BACKGROUND_LOGIN_IMAGE(HotelImageFinal MAIN_BACKGROUND_LOGIN_IMAGE) {
        this.MAIN_BACKGROUND_LOGIN_IMAGE = MAIN_BACKGROUND_LOGIN_IMAGE;
    }
}
