package com.rakosoft.staytechtest.services;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.rakosoft.staytechtest.R;
import com.rakosoft.staytechtest.activities.MainActivity;
import com.rakosoft.staytechtest.beans.hotel.Hotel;
import com.rakosoft.staytechtest.controller.Controller;
import com.rakosoft.staytechtest.network.NetworkConstants;
import com.rakosoft.staytechtest.network.NetworkUtils;
import com.rakosoft.staytechtest.network.request.GetHotelsRequest;
import com.rakosoft.staytechtest.network.response.GetHotelsResponse;
import com.rakosoft.staytechtest.network.services.HotelService;
import com.rakosoft.staytechtest.network.services.ServiceGenerator;
import com.rakosoft.staytechtest.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Joss on 13/06/2016.
 */
public class HotelsService {

    public void processGetHotelsServerCall(GetHotelsResponse getHotelsResponse){
        Controller.getInstance().setHotels(getHotelsResponse.getHotels());
        Collections.sort(Controller.getInstance().getHotels());
        Collections.sort(Controller.getInstance().getPublishedHotels());
        logHotels(Controller.getInstance().getHotels());
    }

    public void logHotels(List<Hotel> hotelList){
        if(hotelList!=null){
            for(Hotel hotel: hotelList){
                Log.i("Hotel",hotel.toString());
            }
        }
    }

    public void callServerGetHotels(final Activity activity){
        HotelService hotelsService = ServiceGenerator.createService(HotelService.class, activity,activity.getResources().getString(R.string.loading));
        Call<GetHotelsResponse> call = hotelsService.getHotels(new GetHotelsRequest());
        call.enqueue(new Callback<GetHotelsResponse>() {
            @Override
            public void onResponse(Call<GetHotelsResponse> call, Response<GetHotelsResponse> response) {
                Log.i(activity.getResources().getString(R.string.retrofit_log), activity.getResources().getString(R.string.retrofit_onResponse, NetworkConstants.CALLSERVERGETHOTELS,activity.getClass().getSimpleName()));
                GetHotelsResponse createLinkObjectResponse = response.body();
                if(createLinkObjectResponse!=null){
                    if(createLinkObjectResponse.isSuccess()){
                        new HotelsService().processGetHotelsServerCall(createLinkObjectResponse);
                        ((MainActivity)activity).showHotels();
                        NetworkUtils.dismissProgress(activity);
                    }else{
                        ArrayList<String> errors = createLinkObjectResponse.getErrors();
                        if(errors!=null){
                            //TODO make this better....
                            for(String error: errors){
                                Toast.makeText(activity.getApplicationContext(),error,Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(activity.getApplicationContext(),activity.getResources().getString(R.string.generic_error),Toast.LENGTH_SHORT).show();
                        }
                    }
                }else{
                    Log.i(activity.getResources().getString(R.string.retrofit_log), activity.getResources().getString(R.string.retrofit_onFailure, NetworkConstants.CALLSERVERGETHOTELS,activity.getClass().getSimpleName()));
                }
                //((MainActivity)activity).hideSoftKeyboard();
            }

            @Override
            public void onFailure(Call<GetHotelsResponse> call, Throwable t) {
                Log.i(activity.getResources().getString(R.string.retrofit_log), activity.getResources().getString(R.string.retrofit_onFailure, NetworkConstants.CALLSERVERGETHOTELS,activity.getClass().getSimpleName()));
                Utils.showInfoDialog(activity.getResources().getString(R.string.retrofit_error, NetworkConstants.CALLSERVERGETHOTELS), activity);
                System.out.println(call);
                //((MainActivity)activity).hideSoftKeyboard();
            }
        });
    }

}
