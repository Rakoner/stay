package com.rakosoft.staytechtest.beans.hotel;

/**
 * Created by Joss on 17/06/2016.
 */
public class HotelImages {

    private HotelImage images;

    public HotelImage getImages() {
        return images;
    }

    public void setImages(HotelImage images) {
        this.images = images;
    }
}
